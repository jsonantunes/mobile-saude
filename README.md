# Challenge Mobile Saude

## Quick Start

> docker-compose up -d

## App

> http://localhost

## API

Lista Regiões
> GET http://localhost/region/index

Lista Estados
> GET http://localhost/state/index

 Lista Cidades
> GET http://localhost/city/filter?region=1&state=1&city=Vitória

## phpMyAdmin
> http://localhost:8090
>
> servidor: mysql
>
> usuario: root
>
> password: challenge

## Pasta Docs

* dump.sql - dump do banco
* der.png - diagrama de entidade relacionameto





