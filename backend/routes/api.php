<?php
/**
 * Api Routes
 *
 * @return array
 */
return [
    'state' => [
        'static' => [
            'index' => [
                'method' => 'get',
                'controller' => '\Challenge\Controller\StateController',
                'action' => 'index'
            ],
        ],
    ],
    'region' => [
        'static' => [
            'index' => [
                'method' => 'get',
                'controller' => '\Challenge\Controller\RegionController',
                'action' => 'index'
            ],
        ]
    ],
    'city' => [
        'static' => [
            'filter' => [
                'method' => 'get',
                'controller' => '\Challenge\Controller\CityController',
                'action' => 'index'
            ],
        ],
    ],
];
