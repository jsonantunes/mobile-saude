<?php

require __DIR__ . '/../vendor/autoload.php';

$container = new \Core\Container();
$router = $container->resolve(\Core\Route::class)->getResolvedObject()->init();
$options = $router->resolveStaticRoute() ?: $router->resolveDynamicRoute();

if ($options) {
    $response = $container->resolve($options['controller'])->invoke($options['action']);
    $response->send();
}
