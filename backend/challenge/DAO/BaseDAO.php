<?php

namespace Challenge\DAO;

use Core\DBConnection;

class BaseDAO extends DBConnection
{
    public function toArray($rows)
    {
        $data = [];
        foreach ($rows as $row) {
            $data[] = $row;
        }
        return $data;
    }
}
