<?php

namespace Challenge\DAO;

use Challenge\DAO\BaseDAO;

class RegionDAO extends BaseDAO
{
    public function all()
    {
        $rows = $this->getQuery('SELECT region.id, region.name FROM region');
        return $this->toArray($rows);
    }
}
