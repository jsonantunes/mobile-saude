<?php

namespace Challenge\DAO;

use Challenge\DAO\BaseDAO;

class CityDAO extends BaseDAO
{
    protected $filters;

    public function filter(array $data)
    {
        if (array_key_exists('region', $data)) {
            $this->filters['bind'][] = 'region_id = ?';
            $this->filters['value'][] = $data['region'];
        }

        if (array_key_exists('state', $data)) {
            $this->filters['bind'][] = 'state_id = ?';
            $this->filters['value'][] = $data['state'];
        }

        if (array_key_exists('city', $data)) {
            $this->filters['bind'][] = 'city.name LIKE ?';
            $this->filters['value'][] = '%'. $data['city'] . '%';
        }

        return $this;
    }

    public function all()
    {
        $query = 'SELECT'
                . ' city.id, city.name, city.reference'
                . ' FROM city '
                . ' JOIN state ON city.state_id = state.id'
                . ' WHERE '
                . implode(' AND ', $this->filters['bind']);

        $stmt = $this->dbc->prepare($query);
        $stmt->execute($this->filters['value']);
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $this->toArray($rows);
    }
}
