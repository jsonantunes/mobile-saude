<?php

namespace Challenge\DAO;

use Challenge\DAO\BaseDAO;

class StateDAO extends BaseDAO
{
    public function all()
    {
        $rows = $this->getQuery('SELECT state.id, state.name FROM state');
        return $this->toArray($rows);
    }
}
