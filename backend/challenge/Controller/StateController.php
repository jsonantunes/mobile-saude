<?php

namespace Challenge\Controller;

use Core\Request;
use Core\Response;
use Challenge\DAO\StateDAO;

class StateController
{
    protected $stateDAO;
    protected $response;

    public function __construct(StateDAO $stateDAO, Response $response)
    {
        $this->stateDAO = $stateDAO;
        $this->response = $response;
    }

    public function index()
    {
        $states = $this->stateDAO->all();

        return $this->response
                    ->json($states)
                    ->status(200);
    }
}
