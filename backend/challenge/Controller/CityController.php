<?php

namespace Challenge\Controller;

use Core\Request;
use Core\Response;
use Challenge\DAO\CityDAO;

class CityController
{
    protected $cityDAO;
    protected $response;

    public function __construct(CityDAO $cityDAO, Response $response)
    {
        $this->cityDAO = $cityDAO;
        $this->response = $response;
    }

    public function index(Request $request)
    {
        $regions = $this->cityDAO
                        ->filter($request->all())
                        ->all();

        return $this->response
                    ->json($regions)
                    ->status(200);
    }
}
