<?php

namespace Challenge\Controller;

use Core\Request;
use Core\Response;
use Challenge\DAO\RegionDAO;

class RegionController
{
    protected $regionDAO;
    protected $response;

    public function __construct(RegionDAO $regionDAO, Response $response)
    {
        $this->regionDAO = $regionDAO;
        $this->response = $response;
    }

    public function index()
    {
        $regions = $this->regionDAO->all();

        return $this->response
                    ->json($regions)
                    ->status(200);
    }
}
