<?php

/**
 * Challenge - A simple app
 *
 * This file is based in laravel file server.php,
 * allows us to emulate Apache's "mod_rewrite" functionality from the
 * built-in PHP web server.
 *
 * @package Challenge
 * @author  Johnson Antunes <johnsonantuness@gmail.com>
 */

$uri = urldecode(
    parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
);

if ($uri !== '/' && file_exists(__DIR__.'/public'.$uri)) {
    return false;
}

require __DIR__ . '/public/index.php';
