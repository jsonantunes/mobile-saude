<?php

namespace Core;

use Core\Config;

/**
 * Credits for implementarion
 *
 * Class DBConnection
 * Create a database connection using PDO
 *
 * @author jonahlyn@unm.edu
 */
class DBConnection
{
    protected $config;
    public $dbc;

    /**
     * Constructor
     *
     * Opens the database connection
     */
    public function __construct()
    {
        $this->config = Config::database();
        $this->_getPDOConnection();
    }

    /**
     * Function __destruct
     * Closes the database connection
     */
    public function __destruct()
    {
        $this->dbc = null;
    }

    /**
     * Function _getPDOConnection
     *
     * Get a connection to the database using PDO.
     */
    private function _getPDOConnection()
    {
        // Check if the connection is already established
        if ($this->dbc == null) {
            // Create the connection
            $dsn = "" .
                $this->config['driver'] .
                ":host=" . $this->config['host'] .
                ";dbname=" . $this->config['dbname'];
            try {
                $this->dbc = new \PDO($dsn, $this->config['username'], $this->config['password'], [
                    \PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET utf8"
                ]);
            } catch(\PDOException $e ) {
                echo __LINE__.$e->getMessage();
            }
        }
    }

    /**
     * Function runQuery
     *
     * Runs a insert, update or delete query
     *
     * @param string $sql insert update or delete statement
     *
     * @return int count of records affected by running the sql statement.
     */
    public function runQuery($sql)
    {
        try {
            $count = $this->dbc->exec($sql) or print_r($this->dbc->errorInfo());
        } catch(\PDOException $e) {
            echo __LINE__.$e->getMessage();
        }
        return $count;
    }

    /**
     * Function getQuery
     *
     * Runs a select query
     *
     * @param string $sql insert update or delete statement
     *
     * @return associative array
     */
    public function getQuery($sql)
    {
        $stmt = $this->dbc->query($sql);
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);

        return $stmt;
    }
}