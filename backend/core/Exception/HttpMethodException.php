<?php

namespace Core\Exception;

class HttpMethodException extends \Exception
{
    public function errorMessage()
    {
        return 'Http method not allowed';
    }
}