<?php

namespace Core;

abstract class Config
{
    private static $routes;
    private static $database;

    public static function routes()
    {
        if (!self::$routes) {
            self::$routes = include '../routes/api.php';
        }

        return self::$routes;
    }

    public static function database()
    {
        if (!self::$database) {
            self::$database = include '../config/database.php';
        }

        return self::$database;
    }
}
