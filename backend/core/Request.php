<?php

namespace Core;

class Request
{
    public function all()
    {
        return $_GET;
    }

    public function uri()
    {
        return $_SERVER['REQUEST_URI'];
    }

    public function method()
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    public function segments()
    {
        return explode('/', strtok($this->uri(), '?'));
    }
}
