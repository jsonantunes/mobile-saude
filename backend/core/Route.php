<?php

namespace Core;

use Core\Request;
use Core\Container;
use Core\Exception\HttpMethodException;

class Route
{
    protected $request;
    protected static $routes;

    public function __construct(Request $request)
    {
        $this->request = $request;
        self::$routes = Config::routes();
    }

    public function init()
    {
        if (!$routes = $this->_resolveBaseRoute()) {
            die('Invalid Router');
        }

        return $this;
    }

    public function resolveStaticRoute()
    {
        $routes = $this->_resolveBaseRoute();

        if (!array_key_exists('static', $routes)) {
            return null;
        }

        if (array_key_exists($this->_strSegmentAfterBaseRoute(), $routes['static'])) {
            return $routes['static'][$this->_strSegmentAfterBaseRoute()];
        }

        return null;
    }

    public function resolveDynamicRoute()
    {
        $routes = $this->_resolveBaseRoute();

        if (!array_key_exists('dynamic', $routes)) {
            return null;
        }

        foreach ($routes['dynamic'] as $dynamicRoute => $options) {
            $resolved = [];
            $split = explode('/', $dynamicRoute);
            if ($this->_validSegments($dynamicRoute)) {
                foreach ($split as $key => $breadcrumb) {
                    $segments = $this->_segmentsAfterBaseRoute();

                    $op = true;
                    do {
                        if ($segments[$key] === $breadcrumb) {
                            $resolved[] = $breadcrumb;
                            $op = false;
                        } elseif (substr($breadcrumb, 0, 1) === '{') {
                            $resolved[] = $segments[$key];
                            $op = false;
                        }
                    } while ($op);
                }

                if (implode('/', $resolved) === $this->_strSegmentAfterBaseRoute()) {
                    return $options;
                }
            }
        }

        return null;
    }

    private function _baseRoute()
    {
        if (array_key_exists($this->request->segments()[1], self::$routes)) {
            $d = $this->request->segments()[1];
            return $d;
        }

        return null;
    }

    private function _segmentsAfterBaseRoute()
    {
        $segments = $this->request->segments();
        return array_slice($segments, 2);
    }

    private function _strSegmentAfterBaseRoute()
    {
        return implode('/', $this->_segmentsAfterBaseRoute());
    }

    private function _resolveBaseRoute()
    {
        return self::$routes[$this->_baseRoute()];
    }

    private function _validSegments($segment)
    {
        $countRequestSegments = count($this->_segmentsAfterBaseRoute());
        $countRouteSegments = count(explode('/', $segment));

        return $countRequestSegments === $countRouteSegments;
    }
}
