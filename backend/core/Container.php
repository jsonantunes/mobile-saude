<?php

namespace Core;

/**
 * A simple and small container
 *
 * Resolve dependency injection on contructor and methods
 *
 * @author Johnson Antunes <johnsonantuness@gmail.com>
 */
class Container
{
    protected static $instance;
    protected $resolvedObject;
    protected $reflectionClass;
    protected $reflectionMethod;

    /**
     * Simple resolution for dependency injection
     *
     * @param string $class
     *
     * @return @var object
     */
    public function resolve(string $class)
    {
        $this->reflectionClass = new \ReflectionClass($class);
        if ($this->reflectionClass->hasMethod('__construct')) {
            $paramsDependency = $this->_resolveMethod('__construct');
            $this->resolvedObject = $this->reflectionClass->newInstanceArgs($paramsDependency);
            return $this;
        }

        $this->resolvedObject = $this->reflectionClass->newInstance();
        return $this;
    }

    /**
     * Method Invoke
     *
     * Use for invoke methods with DI
     *
     * @param string $method
     * @param array $params
     *
     * @return void
     */
    public function invoke(string $method, array $params = [])
    {
        $paramsDependency = $this->_resolveMethod($method);
        return $this->reflectionMethod->invokeArgs($this->resolvedObject, $paramsDependency);
    }

    /**
     * Return concrete object
     *
     * @return object
     */
    public function getResolvedObject()
    {
        return $this->resolvedObject;
    }

    /**
     * Find and Resolve DI
     *
     * @param string $method
     * @return void
     */
    private function _resolveMethod(string $method)
    {
        $this->reflectionMethod = $this->reflectionClass->getMethod($method);
        $parameters = $this->reflectionMethod->getParameters();

        if ($this->reflectionMethod && $parameters) {
            $paramsDependency = [];
            foreach ($parameters as $param) {
                $classDependency = $param->getType()->getName();
                $params = (new Container())->resolve($classDependency);
                $paramsDependency[$param->getName()] = new $classDependency();
            }

            return $paramsDependency;
        }

        return [];
    }
}
