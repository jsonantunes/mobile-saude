<?php

namespace Core;

class Response
{
    protected $data;
    protected $statusCode;

    public function json(array $data = [])
    {
        $this->data = $data;
        return $this;
    }

    public function status(int $code)
    {
        $this->statusCode = $code;
        return $this;
    }

    public function send()
    {
        if ($this->statusCode) {
            header("HTTP/1.1 " . $this->statusCode . " OK");
        }

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        header('Content-type: Application/json');

        echo json_encode($this->data);
    }

    public function getData()
    {
        return $this->data;
    }
}
