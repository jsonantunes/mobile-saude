import http from './http';

const all = () => http.get('region/index').then(({ data }) => data);

export default {
  all,
};
