import http from './http';

const all = (region, state, city) => {
  const params = {};
  if (region) {
    params.region = region;
  }
  if (state) {
    params.state = state;
  }
  if (city) {
    params.city = city;
  }

  return http.get('city/filter', { params: params }).then(({ data }) => data);
};

export default {
  all,
};
