import http from './http';

const all = () => http.get('state/index').then(({ data }) => data);

export default {
  all,
};
